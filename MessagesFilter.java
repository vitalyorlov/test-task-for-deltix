public class MessagesFilter implements FrequencyChecker {

    private int limit;
    Queue<Date> queue = new LinkedList<Date>();

    public MessagesFilter(int limit) {
        this.limit = limit;
    }

    public void isAllowed() {
        queue.add(new Date());
        if(getMessagesCount() <= limit) return true;
            else return false;
    }    

    public int getMessagesCount() {
        collectGarbage();
        return queue.size();
    }

    public void collectGarbage() {
        Iterator x = list.descendingIterator();
        Date now = new Date();

        while (x.hasNext()) {
            if(now.getTime() - x.next().getTime() <= 60000) {
                x.remove();
            } else {
                break;
            }
        }
    }

}
